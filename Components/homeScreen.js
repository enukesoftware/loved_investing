/* @flow */

import React, { Component } from 'react';
import {
	View,
	Text,
	StyleSheet,
	Alert,
	TouchableOpacity,
	Platform,
	AsyncStorage, Modal,
	SafeAreaView,
	Image,
	ScrollView,
} from 'react-native';
import Color from "./../Constants/Color"
import Font from './../Constants/FontSize'
import Styles from './../Constants/Styles'
import Images from './../Constants/Images'
import Swiper from 'react-native-swiper';
import Fonts from '../Constants/Fonts';
import CardView from 'react-native-cardview'
import starImage from './../imgs/star.png';
import downImage from './../imgs/down-button.png';
import upImage from './../assets/images/up.png';
import dotImage from './../imgs/circle.png';
import ProgressCircle from 'react-native-progress-circle'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default class DentistAccount extends Component {

	constructor(props) {
		super(props);
		this.state = {
			modalVisibility: false,
			selectedTab: 0,
			showDescription: false,
			data: [{
				points: ["Low risk", "Mostly bonds with 30% stocks for extra growth"],
				ratio: 30,
				ratioText: "70% Bonds 30% Stocks",
				headLine: "Conservative Mix"
			},
			{
				points: ["Medium risk", "Mostly bonds with 40% stocks for extra growth"],
				ratio: 40,
				ratioText: "60% Bonds 40% Stocks",
				headLine: "Moderate Mix"
			},
			{
				points: ["High risk", "Mostly stocks with some risk protection via bonds"],
				ratio: 80,
				ratioText: "80% Bonds 20% Stocks",
				headLine: "Aggressive Mix"
			},
			{
				points: ["Ultra High risk", "All stocks", "For very long-term savings"],
				ratio: 100,
				ratioText: "100% Stocks",
				headLine: "Ultra Aggressive Mix"
			}
			]
		};
	}
	showModal = (visibility) => {
		this.setState({ modalVisibility: visibility })
	}
	render() {
		return (
			<View style={Styles.container}>
				<TouchableOpacity style={Styles.homeScreenButton} onPress={() => { this.showModal(true) }}>
					<Text style={Styles.mainScreenText}>Continue</Text>
				</TouchableOpacity>
				<Modal animationType="slide" visible={this.state.modalVisibility}>
					<SafeAreaView style={Styles.modalStyle}>

						<View style={{ flexDirection: 'row', height: 40 }}>
							<View style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }}><Image source={Images.backButton} style={Styles.backImage} /></View>
							<View style={{ flex: 6, alignItems: 'center', justifyContent: 'center' }}><Text style={{ color: Color.white, fontFamily: Fonts.bold, fontSize: Font.large }}>College</Text></View>
							<TouchableOpacity style={{ flex: 2, alignItems: 'center', justifyContent: 'center' }} onPress={() => { this.showModal(false) }}><Image source={Images.cancelButton} style={Styles.cancelImage} /></TouchableOpacity>
						</View>
						{this.state.showDescription == false ? <View style={{ width: '80%', height: 5, backgroundColor: Color.yellow, marginTop: 5, borderRadius: 5 }}></View>
							: <View></View>}
						<View style={{ flex: 1 }}>
							<View style={{ flex: 8 }}>{this.state.showDescription == false ? <View><Text style={Styles.portFolioStyle}>Choose a portfolio.</Text>
								<Text style={Styles.portfolioDescription}>
									We’ve recommended a [Short Duration Bond Portfolio] to help you reach your goals sooner.
							</Text></View> : <View></View>}


								<Text style={Styles.riskStyle}>Risk</Text>
								<View style={Styles.tabBar}>
									<TouchableOpacity style={Styles.tabBarFirstChanges} onPress={() => { this.setState({ selectedTab: 0 }) }}><Text style={this.state.selectedTab == 0 ? Styles.tabbarSelectedTextStyle : Styles.tabbarTextStyle}>Very low</Text></TouchableOpacity>
									<TouchableOpacity style={Styles.tabBarData} onPress={() => { this.setState({ selectedTab: 1 }) }}><Text style={this.state.selectedTab == 1 ? Styles.tabbarSelectedTextStyle : Styles.tabbarTextStyle}>Low</Text></TouchableOpacity>
									<TouchableOpacity style={Styles.tabBarData} onPress={() => { this.setState({ selectedTab: 2 }) }}><Text style={this.state.selectedTab == 2 ? Styles.tabbarSelectedTextStyle : Styles.tabbarTextStyle}>Medium</Text></TouchableOpacity>
									<TouchableOpacity style={Styles.tabBarData} onPress={() => { this.setState({ selectedTab: 3 }) }}><Text style={this.state.selectedTab == 3 ? Styles.tabbarSelectedTextStyle : Styles.tabbarTextStyle}>High</Text></TouchableOpacity>
									<TouchableOpacity style={Styles.tabBarData} onPress={() => { this.setState({ selectedTab: 4 }) }}><Text style={this.state.selectedTab == 4 ? Styles.tabbarSelectedTextStyle : Styles.tabbarTextStyle}>Ultra</Text></TouchableOpacity></View>

								<View style={{ width: '100%', height: !this.state.showDescription ? 280 : 500 }}>
									<Swiper showsButtons={false} loop={false} dotColor={Color.white} activeDotColor={Color.yellow} index={this.state.selectedTab} onIndexChanged={(index) => { this.setState({ selectedTab: index }) }}>

										<CardView cardElevation={2} cardMaxElevation={2} cornerRadius={12}
											style={{
												backgroundColor: 'white',
												width: '90%',
												marginLeft: '5%',
												// height: 260
												height: !this.state.showDescription ? 230 : "auto"
											}}>
											<View style={{
												width: "100%", height: 50,
												backgroundColor: "black",
												borderTopLeftRadius: 12,
												borderTopRightRadius: 12,
												flexDirection: "row",
												alignItems: "center"
											}}>
												<View style={{
													height: "100%",
													width: "15%",
													justifyContent: "center",
													alignItems: "center",
													// backgroundColor: "red"
												}}>
													<Image source={starImage} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
												</View>
												<View style={{
													width: "85%", height: "100%",
													justifyContent: "center",
													// backgroundColor: "#00c4f5"
												}}>
													{this.state.showDescription == true ? <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>Short Duration Bond Portfolio</Text> : <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>Ultra Conservative</Text>}

													<Text style={{
														fontWeight: "bold", marginTop: 3,
														color: Color.yellow, fontSize: Font.medium
													}}>Recommended</Text>
												</View>
											</View>
											<View style={{
												width: "100%",
												height: 125,
												flexDirection: "row",
												alignItems: "center",
											}}>
												<View style={{
													width: "55%",
													alignItems: "center",
													flexWrap: "nowrap"
												}}>
													<View style={{
														width: "90%", paddingLeft: 5,
														alignItems: "center", flexDirection: "row"
													}}>
														<Image source={dotImage} style={{ height: 5, width: 5 }}></Image>
														<Text style={this.state.showDescription == true ? {
															//fontWeight: "bold",
															fontFamily: Fonts.bold,
															fontSize: Font.medium, marginLeft: 5,
															flexWrap: "wrap",
														} : {
																//fontWeight: "bold",
																fontFamily: Fonts.medium,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															}}>Very low risk</Text>
													</View>
													<View style={{
														width: "95%",
														paddingLeft: 10,
														alignItems: "center",
														flexDirection: "row",
													}}>
														{this.state.showDescription == true ? <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -15 }}></Image> : <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -15 }}></Image>}
														<View style={{
															width: "95%",
														}}>
															<Text style={this.state.showDescription == true ? {
																//fontWeight: "bold",
																marginTop: 10,
																fontFamily: Fonts.bold,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															} : {
																	//fontWeight: "bold",
																	marginTop: 10,
																	fontFamily: Fonts.medium,
																	fontSize: Font.medium, marginLeft: 5,
																	flexWrap: "wrap",
																}}>100% fixed income short-term bonds for safety</Text>
														</View>
													</View>
												</View>
												<View style={{
													width: "45%", justifyContent: "center",
													alignItems: "center"
												}}>
													<ProgressCircle
														percent={0}
														radius={50}
														borderWidth={15}
														color={Color.lightBlue}
														shadowColor={Color.yellow}
														bgColor="#fff"
													>
														<Text style={{ fontSize: Font.textMedium, fontFamily: Fonts.large, color: "black", flexWrap: 'wrap', paddingLeft: 5, paddingRight: 5, textAlign: 'center' }}>{'100% Bonds'}</Text>

													</ProgressCircle>
												</View>
											</View>
											{this.state.showDescription == false ?
												<TouchableOpacity style={{
													height: 50, width: "100%",
													alignItems: "center",
													justifyContent: "center",
													// backgroundColor: "red"
												}} onPress={() => this.setState({ showDescription: !this.state.showDescription })}>
													<Text style={{ color: Color.lightBlue, fontSize: Font.regular, fontWeight: "500" }}>More Details</Text>
													<Image source={downImage} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
												</TouchableOpacity> :
												<View></View>
											}
											{this.state.showDescription == true ?
												<View style={{
													width: "90%", marginRight: '10%',
													marginLeft: '5%',
													paddingBottom: 10
												}}>
													<Text style={{
														fontSize: Font.medium, fontFamily: Fonts.medium,
														color: "black"
													}}>PIMCO Enhanced Short Maturity Active ETF (MINT) seeks greater income and total return potential than cash and money market funds by investing in a broad range of high-quality short-term instruments.</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Underlying</Text>
													<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>PIMCO Enhanced Short Maturity Active ETF</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Risk</Text>
													<View style={{ width: '100%', backgroundColor: 'lightgray', borderRadius: 20, height: 15, marginTop: 5, justifyContent: 'center' }}>
														<View style={{ width: '85%', backgroundColor: Color.yellow, borderRadius: 20, height: 14 }}></View></View>
													<View style={{ flexDirection: 'row', marginTop: 10 }}>
														<View style={{ flexDirect: 'column' }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Yield</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>2.45%</Text>
														</View>
														<View style={{ flexDirect: 'column', marginLeft: 35 }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Expense Ratio</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>0.25%</Text>
														</View></View>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Link</Text>
													<TouchableOpacity onPress={() => this.setState({ showDescription: !this.state.showDescription })} style={{ marginTop: 10, width: '100%', alignItems: 'center', justifyContent: 'center', height: 20 }}>

														<Image source={upImage} style={{ marginTop: 10, width: 15, height: 15 }} />
													</TouchableOpacity>
												</View> : <View></View>}

										</CardView>










										<CardView cardElevation={2} cardMaxElevation={2} cornerRadius={12}
											style={{
												backgroundColor: 'white',
												width: '90%',
												marginLeft: '5%',
												// height: 260
												height: !this.state.showDescription ? 230 : "auto"
											}}>
											<View style={{
												width: "100%", height: 50,
												backgroundColor: "black",
												borderTopLeftRadius: 12,
												borderTopRightRadius: 12,
												flexDirection: "row",
												alignItems: "center"
											}}>

												<View style={{
													width: "85%", height: "100%",
													justifyContent: "center",
													marginLeft: 20
													// backgroundColor: "#00c4f5"
												}}>
													{this.state.showDescription == true ? <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>Short Duration Bond Portfolio</Text> : <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>{this.state.data[0].headLine}</Text>}

												</View>
											</View>
											<View style={{
												width: "100%",
												height: 125,
												flexDirection: "row",
												alignItems: "center",
											}}>
												<View style={{
													width: "55%",
													alignItems: "center",
													flexWrap: "nowrap"
												}}>
													<View style={{
														width: "90%", paddingLeft: 5,
														alignItems: "center", flexDirection: "row"
													}}>
														<Image source={dotImage} style={{ height: 5, width: 5 }}></Image>
														<Text style={this.state.showDescription == true ? {
															//fontWeight: "bold",
															fontFamily: Fonts.bold,
															fontSize: Font.medium, marginLeft: 5,
															flexWrap: "wrap",
														} : {
																//fontWeight: "bold",
																fontFamily: Fonts.medium,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															}}>{this.state.data[0].points[0]}</Text>
													</View>
													<View style={{
														width: "95%",
														paddingLeft: 10,
														alignItems: "center",
														flexDirection: "row",
													}}>
														{this.state.showDescription == true ? <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -40 }}></Image> : <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -5 }}></Image>}
														<View style={{
															width: "95%",
														}}>
															<Text style={this.state.showDescription == true ? {
																//fontWeight: "bold",
																marginTop: 10,
																fontFamily: Fonts.bold,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															} : {
																	//fontWeight: "bold",
																	marginTop: 10,
																	fontFamily: Fonts.medium,
																	fontSize: Font.medium, marginLeft: 5,
																	flexWrap: "wrap",
																}}>{this.state.data[0].points[1]}</Text>
														</View>
													</View>
												</View>
												<View style={{
													width: "45%", justifyContent: "center",
													alignItems: "center"
												}}>
													<ProgressCircle
														percent={this.state.data[0].ratio}
														radius={50}
														borderWidth={15}
														color={Color.lightBlue}
														shadowColor={Color.yellow}
														bgColor="#fff"
													>
														<Text style={{ fontSize: Font.textMedium, fontFamily: Fonts.large, color: "black", flexWrap: 'wrap', paddingLeft: 5, paddingRight: 5, textAlign: 'center' }}>{this.state.data[0].ratioText}</Text>
													</ProgressCircle>
												</View>
											</View>
											{this.state.showDescription == false ?
												<TouchableOpacity style={{
													height: 50, width: "100%",
													alignItems: "center",
													justifyContent: "center",
													// backgroundColor: "red"
												}} onPress={() => this.setState({ showDescription: !this.state.showDescription })}>
													<Text style={{ color: Color.lightBlue, fontSize: Font.regular, fontWeight: "500" }}>More Details</Text>
													<Image source={downImage} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
												</TouchableOpacity> :
												<View></View>
											}
											{this.state.showDescription == true ?
												<View style={{
													width: "90%", marginRight: '10%',
													marginLeft: '5%',
													paddingBottom: 10
												}}>
													<Text style={{
														fontSize: Font.medium, fontFamily: Fonts.medium,
														color: "black"
													}}>PIMCO Enhanced Short Maturity Active ETF (MINT) seeks greater income and total return potential than cash and money market funds by investing in a broad range of high-quality short-term instruments.</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Underlying</Text>
													<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>PIMCO Enhanced Short Maturity Active ETF</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Risk</Text>
													<View style={{ width: '100%', backgroundColor: 'lightgray', borderRadius: 20, height: 15, marginTop: 5, justifyContent: 'center' }}>
														<View style={{ width: '85%', backgroundColor: Color.yellow, borderRadius: 20, height: 14 }}></View></View>
													<View style={{ flexDirection: 'row', marginTop: 10 }}>
														<View style={{ flexDirect: 'column' }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Yield</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>2.45%</Text>
														</View>
														<View style={{ flexDirect: 'column', marginLeft: 35 }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Expense Ratio</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>0.25%</Text>
														</View></View>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Link</Text>
													<TouchableOpacity onPress={() => this.setState({ showDescription: !this.state.showDescription })} style={{ marginTop: 10, width: '100%', alignItems: 'center', justifyContent: 'center', height: 20 }}>

														<Image source={upImage} style={{ marginTop: 10, width: 15, height: 15 }} />
													</TouchableOpacity>
												</View> : <View></View>}

										</CardView>


										<CardView cardElevation={2} cardMaxElevation={2} cornerRadius={12}
											style={{
												backgroundColor: 'white',
												width: '90%',
												marginLeft: '5%',
												// height: 260
												height: !this.state.showDescription ? 230 : "auto"
											}}>
											<View style={{
												width: "100%", height: 50,
												backgroundColor: "black",
												borderTopLeftRadius: 12,
												borderTopRightRadius: 12,
												flexDirection: "row",
												alignItems: "center"
											}}>

												<View style={{
													width: "85%", height: "100%",
													justifyContent: "center",
													marginLeft: 20
													// backgroundColor: "#00c4f5"
												}}>
													{this.state.showDescription == true ? <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>Short Duration Bond Portfolio</Text> : <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>{this.state.data[1].headLine}</Text>}

												</View>
											</View>
											<View style={{
												width: "100%",
												height: 125,
												flexDirection: "row",
												alignItems: "center",
											}}>
												<View style={{
													width: "55%",
													alignItems: "center",
													flexWrap: "nowrap"
												}}>
													<View style={{
														width: "90%", paddingLeft: 5,
														alignItems: "center", flexDirection: "row"
													}}>
														<Image source={dotImage} style={{ height: 5, width: 5 }}></Image>
														<Text style={this.state.showDescription == true ? {
															//fontWeight: "bold",
															fontFamily: Fonts.bold,
															fontSize: Font.medium, marginLeft: 5,
															flexWrap: "wrap",
														} : {
																//fontWeight: "bold",
																fontFamily: Fonts.medium,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															}}>{this.state.data[1].points[0]}</Text>
													</View>
													<View style={{
														width: "95%",
														paddingLeft: 10,
														alignItems: "center",
														flexDirection: "row",
													}}>
														{this.state.showDescription == true ? <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -40 }}></Image> : <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -5 }}></Image>}
														<View style={{
															width: "95%",
														}}>
															<Text style={this.state.showDescription == true ? {
																//fontWeight: "bold",
																marginTop: 10,
																fontFamily: Fonts.bold,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															} : {
																	//fontWeight: "bold",
																	marginTop: 10,
																	fontFamily: Fonts.medium,
																	fontSize: Font.medium, marginLeft: 5,
																	flexWrap: "wrap",
																}}>{this.state.data[1].points[1]}</Text>
														</View>
													</View>
												</View>
												<View style={{
													width: "45%", justifyContent: "center",
													alignItems: "center"
												}}>
													<ProgressCircle
														percent={this.state.data[1].ratio}
														radius={50}
														borderWidth={15}
														color={Color.lightBlue}
														shadowColor={Color.yellow}
														bgColor="#fff"
													>
														<Text style={{ fontSize: Font.textMedium, fontFamily: Fonts.large, color: "black", flexWrap: 'wrap', paddingLeft: 5, paddingRight: 5, textAlign: 'center' }}>{this.state.data[1].ratioText}</Text>
													</ProgressCircle>
												</View>
											</View>
											{this.state.showDescription == false ?
												<TouchableOpacity style={{
													height: 50, width: "100%",
													alignItems: "center",
													justifyContent: "center",
													// backgroundColor: "red"
												}} onPress={() => this.setState({ showDescription: !this.state.showDescription })}>
													<Text style={{ color: Color.lightBlue, fontSize: Font.regular, fontWeight: "500" }}>More Details</Text>
													<Image source={downImage} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
												</TouchableOpacity> :
												<View></View>
											}
											{this.state.showDescription == true ?
												<View style={{
													width: "90%", marginRight: '10%',
													marginLeft: '5%',
													paddingBottom: 10
												}}>
													<Text style={{
														fontSize: Font.medium, fontFamily: Fonts.medium,
														color: "black"
													}}>PIMCO Enhanced Short Maturity Active ETF (MINT) seeks greater income and total return potential than cash and money market funds by investing in a broad range of high-quality short-term instruments.</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Underlying</Text>
													<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>PIMCO Enhanced Short Maturity Active ETF</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Risk</Text>
													<View style={{ width: '100%', backgroundColor: 'lightgray', borderRadius: 20, height: 15, marginTop: 5, justifyContent: 'center' }}>
														<View style={{ width: '85%', backgroundColor: Color.yellow, borderRadius: 20, height: 14 }}></View></View>
													<View style={{ flexDirection: 'row', marginTop: 10 }}>
														<View style={{ flexDirect: 'column' }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Yield</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>2.45%</Text>
														</View>
														<View style={{ flexDirect: 'column', marginLeft: 35 }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Expense Ratio</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>0.25%</Text>
														</View></View>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Link</Text>
													<TouchableOpacity onPress={() => this.setState({ showDescription: !this.state.showDescription })} style={{ marginTop: 10, width: '100%', alignItems: 'center', justifyContent: 'center', height: 20 }}>

														<Image source={upImage} style={{ marginTop: 10, width: 15, height: 15 }} />
													</TouchableOpacity>
												</View> : <View></View>}

										</CardView>



										<CardView cardElevation={2} cardMaxElevation={2} cornerRadius={12}
											style={{
												backgroundColor: 'white',
												width: '90%',
												marginLeft: '5%',
												// height: 260
												height: !this.state.showDescription ? 230 : "auto"
											}}>
											<View style={{
												width: "100%", height: 50,
												backgroundColor: "black",
												borderTopLeftRadius: 12,
												borderTopRightRadius: 12,
												flexDirection: "row",
												alignItems: "center"
											}}>

												<View style={{
													width: "85%", height: "100%",
													justifyContent: "center",
													marginLeft: 20
													// backgroundColor: "#00c4f5"
												}}>
													{this.state.showDescription == true ? <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>Short Duration Bond Portfolio</Text> : <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>{this.state.data[2].headLine}</Text>}


												</View>
											</View>
											<View style={{
												width: "100%",
												height: 125,
												flexDirection: "row",
												alignItems: "center",
											}}>
												<View style={{
													width: "55%",
													alignItems: "center",
													flexWrap: "nowrap"
												}}>
													<View style={{
														width: "90%", paddingLeft: 5,
														alignItems: "center", flexDirection: "row"
													}}>
														<Image source={dotImage} style={{ height: 5, width: 5 }}></Image>
														<Text style={this.state.showDescription == true ? {
															//fontWeight: "bold",
															fontFamily: Fonts.bold,
															fontSize: Font.medium, marginLeft: 5,
															flexWrap: "wrap",
														} : {
																//fontWeight: "bold",
																fontFamily: Fonts.medium,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															}}>{this.state.data[2].points[0]}</Text>
													</View>
													<View style={{
														width: "95%",
														paddingLeft: 10,
														alignItems: "center",
														flexDirection: "row",
													}}>
														{this.state.showDescription == true ? <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -40 }}></Image> : <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -5 }}></Image>}
														<View style={{
															width: "95%",
														}}>
															<Text style={this.state.showDescription == true ? {
																//fontWeight: "bold",
																marginTop: 10,
																fontFamily: Fonts.bold,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															} : {
																	//fontWeight: "bold",
																	marginTop: 10,
																	fontFamily: Fonts.medium,
																	fontSize: Font.medium, marginLeft: 5,
																	flexWrap: "wrap",
																}}>{this.state.data[2].points[1]}</Text>
														</View>
													</View>
												</View>
												<View style={{
													width: "45%", justifyContent: "center",
													alignItems: "center"
												}}>
													<ProgressCircle
														percent={this.state.data[2].ratio}
														radius={50}
														borderWidth={15}
														color={Color.lightBlue}
														shadowColor={Color.yellow}
														bgColor="#fff"
													>
														<Text style={{ fontSize: Font.textMedium, fontFamily: Fonts.large, color: "black", flexWrap: 'wrap', paddingLeft: 5, paddingRight: 5, textAlign: 'center' }}>{this.state.data[2].ratioText}</Text>
													</ProgressCircle>
												</View>
											</View>
											{this.state.showDescription == false ?
												<TouchableOpacity style={{
													height: 50, width: "100%",
													alignItems: "center",
													justifyContent: "center",
													// backgroundColor: "red"
												}} onPress={() => this.setState({ showDescription: !this.state.showDescription })}>
													<Text style={{ color: Color.lightBlue, fontSize: Font.regular, fontWeight: "500" }}>More Details</Text>
													<Image source={downImage} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
												</TouchableOpacity> :
												<View></View>
											}
											{this.state.showDescription == true ?
												<View style={{
													width: "90%", marginRight: '10%',
													marginLeft: '5%',
													paddingBottom: 10
												}}>
													<Text style={{
														fontSize: Font.medium, fontFamily: Fonts.medium,
														color: "black"
													}}>PIMCO Enhanced Short Maturity Active ETF (MINT) seeks greater income and total return potential than cash and money market funds by investing in a broad range of high-quality short-term instruments.</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Underlying</Text>
													<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>PIMCO Enhanced Short Maturity Active ETF</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Risk</Text>
													<View style={{ width: '100%', backgroundColor: 'lightgray', borderRadius: 20, height: 15, marginTop: 5, justifyContent: 'center' }}>
														<View style={{ width: '85%', backgroundColor: Color.yellow, borderRadius: 20, height: 14 }}></View></View>
													<View style={{ flexDirection: 'row', marginTop: 10 }}>
														<View style={{ flexDirect: 'column' }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Yield</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>2.45%</Text>
														</View>
														<View style={{ flexDirect: 'column', marginLeft: 35 }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Expense Ratio</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>0.25%</Text>
														</View></View>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Link</Text>
													<TouchableOpacity onPress={() => this.setState({ showDescription: !this.state.showDescription })} style={{ marginTop: 10, width: '100%', alignItems: 'center', justifyContent: 'center', height: 20 }}>

														<Image source={upImage} style={{ marginTop: 10, width: 15, height: 15 }} />
													</TouchableOpacity>
												</View> : <View></View>}

										</CardView>


										<CardView cardElevation={2} cardMaxElevation={2} cornerRadius={12}
											style={{
												backgroundColor: 'white',
												width: '90%',
												marginLeft: '5%',
												// height: 260
												height: !this.state.showDescription ? 230 : "auto"
											}}>
											<View style={{
												width: "100%", height: 50,
												backgroundColor: "black",
												borderTopLeftRadius: 12,
												borderTopRightRadius: 12,
												flexDirection: "row",
												alignItems: "center"
											}}>

												<View style={{
													width: "85%", height: "100%",
													justifyContent: "center",
													marginLeft: 20
													// backgroundColor: "#00c4f5"
												}}>
													{this.state.showDescription == true ? <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>Short Duration Bond Portfolio</Text> : <Text style={{
														fontWeight: "bold",
														color: "white", fontSize: Font.large
													}}>{this.state.data[3].headLine}</Text>}


												</View>
											</View>
											<View style={{
												width: "100%",
												height: 125,
												flexDirection: "row",
												alignItems: "center",
											}}>
												<View style={{
													width: "55%",
													alignItems: "center",
													flexWrap: "nowrap"
												}}>
													<View style={{
														width: "90%", paddingLeft: 5,
														alignItems: "center", flexDirection: "row"
													}}>
														<Image source={dotImage} style={{ height: 5, width: 5 }}></Image>
														<Text style={this.state.showDescription == true ? {
															//fontWeight: "bold",
															fontFamily: Fonts.bold,
															fontSize: Font.medium, marginLeft: 5,
															flexWrap: "wrap",
														} : {
																//fontWeight: "bold",
																fontFamily: Fonts.medium,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															}}>{this.state.data[3].points[0]}</Text>
													</View>
													<View style={{
														width: "95%",
														paddingLeft: 10,
														alignItems: "center",
														flexDirection: "row",
													}}>
														{this.state.showDescription == true ? <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -40 }}></Image> : <Image source={dotImage} style={{ height: 5, width: 5, marginTop: 10 }}></Image>}
														<View style={{
															width: "95%",
														}}>
															<Text style={this.state.showDescription == true ? {
																//fontWeight: "bold",
																marginTop: 10,
																fontFamily: Fonts.bold,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															} : {
																	//fontWeight: "bold",
																	marginTop: 10,
																	fontFamily: Fonts.medium,
																	fontSize: Font.medium, marginLeft: 5,
																	flexWrap: "wrap",
																}}>{this.state.data[3].points[1]}</Text>
														</View>
													</View>
													<View style={{
														width: "95%",
														paddingLeft: 10,
														alignItems: "center",
														flexDirection: "row",
													}}>
														{this.state.showDescription == true ? <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -40 }}></Image> : <Image source={dotImage} style={{ height: 5, width: 5, marginTop: -5 }}></Image>}
														<View style={{
															width: "95%",
														}}>
															<Text style={this.state.showDescription == true ? {
																//fontWeight: "bold",
																marginTop: 10,
																fontFamily: Fonts.bold,
																fontSize: Font.medium, marginLeft: 5,
																flexWrap: "wrap",
															} : {
																	//fontWeight: "bold",
																	marginTop: 10,
																	fontFamily: Fonts.medium,
																	fontSize: Font.medium, marginLeft: 5,
																	flexWrap: "wrap",
																}}>{this.state.data[3].points[2]}</Text>
														</View>
													</View>
												</View>
												<View style={{
													width: "45%", justifyContent: "center",
													alignItems: "center"
												}}>
													<ProgressCircle
														percent={this.state.data[3].ratio}
														radius={50}
														borderWidth={15}
														color={Color.lightBlue}
														shadowColor={Color.yellow}
														bgColor="#fff"
													>
														<Text style={{ fontSize: Font.textMedium, fontFamily: Fonts.large, color: "black", flexWrap: 'wrap', paddingLeft: 5, paddingRight: 5, textAlign: 'center' }}>{this.state.data[3].ratioText}</Text>
													</ProgressCircle>
												</View>
											</View>
											{this.state.showDescription == false ?
												<TouchableOpacity style={{
													height: 50, width: "100%",
													alignItems: "center",
													justifyContent: "center",
													// backgroundColor: "red"
												}} onPress={() => this.setState({ showDescription: !this.state.showDescription })}>
													<Text style={{ color: Color.lightBlue, fontSize: Font.regular, fontWeight: "500" }}>More Details</Text>
													<Image source={downImage} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
												</TouchableOpacity> :
												<View></View>
											}
											{this.state.showDescription == true ?
												<View style={{
													width: "90%", marginRight: '10%',
													marginLeft: '5%',
													paddingBottom: 10
												}}>
													<Text style={{
														fontSize: Font.medium, fontFamily: Fonts.medium,
														color: "black"
													}}>PIMCO Enhanced Short Maturity Active ETF (MINT) seeks greater income and total return potential than cash and money market funds by investing in a broad range of high-quality short-term instruments.</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Underlying</Text>
													<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>PIMCO Enhanced Short Maturity Active ETF</Text>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Risk</Text>
													<View style={{ width: '100%', backgroundColor: 'lightgray', borderRadius: 20, height: 15, marginTop: 5, justifyContent: 'center' }}>
														<View style={{ width: '85%', backgroundColor: Color.yellow, borderRadius: 20, height: 14 }}></View></View>
													<View style={{ flexDirection: 'row', marginTop: 10 }}>
														<View style={{ flexDirect: 'column' }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Yield</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>2.45%</Text>
														</View>
														<View style={{ flexDirect: 'column', marginLeft: 35 }}>
															<Text style={{ fontFamily: Fonts.bold, fontSize: Font.medium }}>
																Expense Ratio</Text>
															<Text style={{ fontFamily: Fonts.medium, fontSize: Font.medium }}>0.25%</Text>
														</View></View>
													<Text style={{ marginTop: 10, fontFamily: Fonts.bold, fontSize: Font.medium }}>
														Link</Text>
													<TouchableOpacity onPress={() => this.setState({ showDescription: !this.state.showDescription })} style={{ marginTop: 10, width: '100%', alignItems: 'center', justifyContent: 'center', height: 20 }}>

														<Image source={upImage} style={{ marginTop: 10, width: 15, height: 15 }} />
													</TouchableOpacity>
												</View> : <View></View>}

										</CardView>
									</Swiper>
								</View></View>
							<View style={{ flex: 2, justifyContent: 'flex-end' }}>
								<TouchableOpacity onPress={() => { this.showModal(false) }} style={{ marginBottom: 10, width: '80%', borderRadius: 40, backgroundColor: Color.white, height: 45, marginLeft: '10%', alignItems: 'center', justifyContent: 'center' }}>
									<Text style={{ color: Color.lightBlue, fontFamily: Fonts.medium, fontSize: Font.medium }} onPress={() => { this.showModal(false) }}>Confirm Portfolio</Text></TouchableOpacity>
							</View>


						</View>
					</SafeAreaView>
				</Modal>
			</View >
		);
	}
}

const css = StyleSheet.create({

});

