import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default Color = {
	extraSmall: RFValue(7),
	medium: RFValue(13),
	regular: RFValue(11),
	small: RFValue(9),
	large: RFValue(15),
	extraLarge: RFValue(24),
	textMedium: RFValue(12.5)
};