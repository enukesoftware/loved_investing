import { StyleSheet, Platform } from 'react-native';
import Color from './Color';
import FontSize from './FontSize';
import Fonts from './Fonts';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default Styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center',
		marginBottom: 50
	},
	homeScreenButton: {
		width: '50%',
		height: 50,
		backgroundColor: Color.lightBlue,
		borderRadius: 30,
		justifyContent: 'center',
		alignItems: 'center'
	},
	mainScreenText: {
		color: 'white',
		fontSize: FontSize.large,
		fontFamily: Fonts.bold
	},
	modalStyle: {
		backgroundColor: Color.lightBlue, flex: 1
	},
	backImage: {
		height: "50%",
		width: "50%",
		resizeMode: 'contain'
	},
	cancelImage: {
		height: "50%",
		width: "50%",
		resizeMode: 'contain'
	},
	portFolioStyle: {
		color: Color.white,
		fontFamily: Fonts.medium,
		fontSize: FontSize.extraLarge,
		marginTop: "7%",
		marginLeft: '7%'
	},
	portfolioDescription: {
		flexWrap: "wrap",
		width: "86%",
		marginLeft: "7%",
		color: Color.white,
		fontFamily: Fonts.regular,
		marginTop: '3%',
		fontSize: FontSize.medium
	},
	riskStyle: {
		fontFamily: Fonts.bold,
		marginTop: '2%',
		marginLeft: '7%',
		color: Color.white
	},
	tabBar: {
		flexDirection: 'row',
		width: '90%',
		marginLeft: '5%',
		height: 20,
		marginBottom: 10,
		marginTop: 10
	},
	tabBarData: {
		alignItems: 'center',
		justifyContent: 'center',
		flex: 2,
	},
	tabBarFirstChanges: {
		flex: 2.2,
		alignItems: 'center',
		justifyContent: 'center',
	},
	tabbarTextStyle: {
		color: Color.white,
		fontFamily: Fonts.medium,
		fontSize: FontSize.medium
	},
	tabbarSelectedTextStyle: {
		color: Color.yellow,
		fontFamily: Fonts.bold,
		fontSize: FontSize.medium,
		textDecorationLine: 'underline'
	}
})