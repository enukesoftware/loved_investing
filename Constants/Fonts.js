export default Fonts = {
	bold: "MavenPro-Bold",
	regular: "MavenPro-Regular",
	medium: "MavenPro-Medium",
	black: "MavenPro-Black",
	semiBold: "Montserrat-SemiBold"
};